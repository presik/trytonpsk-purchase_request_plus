# This file is part of purchase_discount module for Tryton.
# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from datetime import date, timedelta
from decimal import Decimal

from trytond.model import ModelView, fields
from trytond.pool import Pool, PoolMeta
from trytond.pyson import Eval
from trytond.transaction import Transaction
from trytond.wizard import Button, StateTransition, StateView, Wizard

STATES = {
    'readonly': Eval('state') != 'draft',
}


class PurchaseRequest(metaclass=PoolMeta):
    __name__ = 'purchase.request'
    reference = fields.Function(fields.Char('Reference'), 'get_reference')
    info_product = fields.Function(fields.Char('Info Product'),
        'get_info_product')
    current_stock = fields.Function(fields.Float('Stock Quantity',
        digits=(16, 2), depends=['product']), 'on_change_with_stock_quantity')
    cost_price_w_tax = fields.Function(fields.Numeric('Cost Price W Tax',
        digits=(16, 2)), 'get_cost_price_w_tax')
    sale_history = fields.Function(fields.Numeric('Sale History'),
        'get_sale_history')

    @classmethod
    def __setup__(cls):
        super(PurchaseRequest, cls).__setup__()
        cls._order.insert(0, ('product.name', 'ASC'))
        cls._order.insert(1, ('product.code', 'ASC'))
        cls._order.insert(2, ('product.name', 'DESC'))
        cls._order.insert(3, ('product.code', 'DESC'))
        cls._buttons.update({
            'wizard_see_stock': {},
        })

    @classmethod
    @ModelView.button_action('purchase_request_plus.wizard_see_stock')
    def wizard_see_stock(cls, records):
        pass

    def get_info_product(self, name):
        if self.product:
            return self.product.description

    def get_reference(self, name):
        if self.product:
            return self.product.template.reference

    def get_cost_price_w_tax(self, name):
        res = 0
        if not self.product:
            return
        pdt = self.product.template
        taxes = pdt.account_category.supplier_taxes_used
        for tax in taxes:
            if tax.rate and tax.rate > 0:
                res = pdt.cost_price * (1 + tax.rate)
        return res

    @fields.depends('product')
    def on_change_with_stock_quantity(self, name=None):
        res = 0
        Location = Pool().get('stock.location')
        locations = Location.search([
            ('type', '=', 'warehouse'),
        ])
        locations_ids = [l.id for l in locations]

        stock_context = {
            'stock_date_end': date.today(),
            'locations': [l.id for l in locations],
        }

        if self.product:
            with Transaction().set_context(stock_context):
                for location_id in locations_ids:
                    try:
                        res_dict = self.product._get_quantity(
                            [self.product],
                            'quantity',
                            [location_id],
                            grouping_filter=([self.product.id],),
                        )
                        if res_dict.get(self.product.id):
                            res += res_dict[self.product.id]
                    except AttributeError as error:
                        print(error)
        return res

    def get_sale_history(self, name):
        SaleLine = Pool().get('sale.line')
        configuration = Pool().get('purchase.configuration')(1)
        if not self.product or self.state != 'draft':
            return

        res = 0
        limit_days = configuration.requisition_history_sale
        start_date = date.today() - timedelta(days=limit_days)
        lines = SaleLine.search_read([
            ('product', '=', self.product.id),
            ('sale.sale_date', '>=', start_date),
            ('sale.state', 'in', ['processing', 'done']),
            ('type', '=', 'line'),
        ], fields_names=['quantity'])

        res = sum([line['quantity'] for line in lines])
        return Decimal(res)


class SeeStockList(ModelView):
    "See Stock List"
    __name__ = 'purchase_request_plus.see_stock_list'
    product = fields.Many2One('product.product', 'Product', states={
        'readonly': True,
    })
    purchase_request = fields.Many2One('purchase.request', 'Purchase Request')
    locations = fields.One2Many('stock.location', None, 'Locations',
        context={
            'product': Eval('product'),
            'context_model': 'product.by_location.context',
        })
    stock_date_end = fields.Date('At Date', readonly=False)

    @staticmethod
    def default_stock_date_end():
        Date = Pool().get('ir.date')
        return Date.today()

    def get_product(self, name=None):
        if self.purchase_request:
            return self.purchase_request.product.id


class WizardSeeStock(Wizard):
    "Wizard See Stock"
    __name__ = 'purchase_request_plus.wizard_see_stock_list'
    start = StateView('purchase_request_plus.see_stock_list',
        'purchase_request_plus.see_stock_view_list', [
            Button('Close', 'close_', 'tryton-cancel'),
        ])
    close_ = StateTransition()

    def default_start(self, fields):
        request_id = Transaction().context.get('active_id')
        Request = Pool().get('purchase.request')
        Locations = Pool().get('stock.location')
        request, = Request.browse([request_id])
        locations = Locations.search([
            ('type', '=', 'storage'),
        ])
        locations_ids = [l.id for l in locations if l.quantity != 0]
        return {
            'product': request.product.id,
            'locations': locations_ids,
            'purchase_request': request_id,
        }

    def transition_close_(self):
        return 'end'


class StockSupplyStart(metaclass=PoolMeta):
    __name__ = 'stock.supply.start'
    kind = fields.Selection([
            ('both', "Both"),
            ('internal', "Internal"),
            ('purchase', "Purchase"),
        ], "Kind", required=True)

    @staticmethod
    def default_kind():
        return 'both'


class StockSupply(metaclass=PoolMeta):
    __name__ = 'stock.supply'

    @property
    def _purchase_parameters(self):
        OrderPoint = Pool().get('stock.order_point')
        domain = []
        if self.start.kind != 'both':
            domain.append(('type', '=', self.start.kind))
        ops = OrderPoint.search(domain)
        products = {'products': [op.product for op in ops]}
        # return {}
        return products

    def generate_purchase(self, clean):
        if self.start.kind not in ['both', 'purchase']:
            return False
        # super(StockSupply, self).generate_purchase(**self._purchase_parameters)
        super(StockSupply, self).generate_purchase(clean)

    def generate_internal(self, clean):
        if self.start.kind not in ['both', 'internal']:
            return False
        super(StockSupply, self).generate_internal(clean)

    def transition_internal(self):
        if self.start.kind in ['both', 'internal']:
            return self.next_action('internal')
        return 'end'

    def transition_purchase(self):
        if self.start.kind in ['both', 'purchase']:
            return self.next_action('purchase')
        return 'end'
