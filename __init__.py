# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import Pool

from . import configuration, purchase_request


def register():
    Pool.register(
        configuration.Configuration,
        purchase_request.PurchaseRequest,
        purchase_request.StockSupplyStart,
        purchase_request.SeeStockList,
        module='purchase_request_plus', type_='model')
    Pool.register(
        purchase_request.WizardSeeStock,
        purchase_request.StockSupply,
        module='purchase_request_plus', type_='wizard')
